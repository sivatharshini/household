import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppliancesComponent } from './appliances/appliances.component';
import { ItemComponent } from './item/item.component';


const routes: Routes = [{ path: '', redirectTo: 'home', pathMatch: 'full'},
{ path: 'home', component: AppliancesComponent },
{ path: 'item', component: ItemComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
