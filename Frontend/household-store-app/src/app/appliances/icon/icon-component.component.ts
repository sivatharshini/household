// src/app/red-component/red-component.component.ts
import {Component} from "@angular/core";
import { DataService } from '../../data.service';

@Component({
    selector: 'app-icon-component',
    templateUrl: './icon-component.component.html'
})
export class IconComponentComponent {
    private params: any;
    constructor(private dataService: DataService) { }
    agInit(params: any): void {
        this.params = params;
    }

    public onRemove(){
        this.dataService.sendDeleteRequest(this.params.data.id).subscribe((data: any[])=>{
            this.params.context.componentParent.loadItems();
        }
        );
       
      }
}