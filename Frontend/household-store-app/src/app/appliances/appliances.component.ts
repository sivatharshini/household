import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { IconComponentComponent } from "./icon/icon-component.component";

@Component({
  selector: 'app-appliances',
  templateUrl: './appliances.component.html',
  styleUrls: ['./appliances.component.css']
})
export class AppliancesComponent implements OnInit {
  householdItems = [];
  context = null;
  defaultColDef =  {
    resizable: true,
    sortable: true,
    filter: true,
    editable: true, 
    singleClickEdit: true,
    suppressCellSelection: true,
    minWidth: 70
  };
  columnDefs = [
		{headerName: 'Serial', field: 'serial_no', flex: 3 , filter: true, },
		{headerName: 'Brand', field: 'brand',  flex: 3, },
    {headerName: 'Model', field: 'model', flex: 3,},
    {headerName: 'Status', field: 'status',  flex: 2,},
    {headerName: 'Date Bought', field: 'date_bought',  flex: 2,},
    {
      field: "", 
      filter: false,
      editable: false, 
      sortable: false,
      flex: 1,
      cellClass: 'no-border',
      cellRendererFramework: IconComponentComponent
    }
	];

  constructor(private dataService: DataService) {
    this.context= {
      componentParent: this
  }
   }

  ngOnInit() {
    this.loadItems();
  }

  public loadItems() {
    this.dataService.sendGetRequest().subscribe((data: any[])=>{  
			console.log(data);  
      this.householdItems = data;
    }
    );
  }
  
  public onCellValueChanged(params) {
    this.dataService.sendPutRequest(params.data).subscribe((data: any[])=>{  
      console.log(data); 
      this.loadItems();
    }
    );
  }

}
