import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators  } from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  addItem = new FormGroup({
    serial_no: new FormControl('',Validators.required),
    brand: new FormControl('',Validators.required),
    model: new FormControl('',Validators.required),
    status: new FormControl('',Validators.required),
    date_bought:new FormControl('',Validators.required),
  });

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.addItem);
    if (this.addItem.invalid) {
      return;
    }
    this.dataService.sendPostRequest(this.addItem.value).subscribe((data: any[])=>{  
      console.log(data); 
      if(data.includes("FOUND")){
        alert('This item is already');
      }
    }
    );
}

}
