import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER = "http://localhost:8080/household/";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public sendGetRequest(){
    return this.httpClient.get(this.REST_API_SERVER).pipe(catchError(this.handleError));
  }

   public sendPostRequest(data){
    return this.httpClient.post(this.REST_API_SERVER,data).pipe(catchError(this.handleError));
  }

  public sendPutRequest(data: Object){
    return this.httpClient.put(this.REST_API_SERVER,data).pipe(catchError(this.handleError));
  }

  public sendDeleteRequest(id: number){
    return this.httpClient.delete(this.REST_API_SERVER + id).pipe(catchError(this.handleError));
  }

  
}
