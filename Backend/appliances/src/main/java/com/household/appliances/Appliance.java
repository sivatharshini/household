package com.household.appliances;

public class Appliance {

	private long id;
	private String serial_no;
	private String brand;
	private String model;
	private String date_bought;
	private String status;
	
	public Appliance() {}

	public Appliance(long id, String serial_no, String brand, String model, String date_bought, String status) {
		super();
		this.id = id;
		this.serial_no = serial_no;
		this.brand = brand;
		this.model = model;
		this.date_bought = date_bought;
		this.status = status;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setSerial_no(String serial_no) {
		this.serial_no = serial_no;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setDate_bought(String date_bought) {
		this.date_bought = date_bought;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSerial_no() {
		return serial_no;
	}

	public String getBrand() {
		return brand;
	}

	public String getModel() {
		return model;
	}

	public String getDate_bought() {
		return date_bought;
	}

	public String getStatus() {
		return status;
	}

	public long getId() {
		return id;
	}

}
