package com.household.appliances;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin( maxAge = 3600)
@RestController
public class AppliancesController {
	
	private final AtomicLong counter = new AtomicLong();
	
	private List<Appliance> availableItems = new ArrayList<>(Arrays.asList(
			new Appliance(counter.incrementAndGet(), "4543543121", "Brita", "BA550", "20/01/2000", "Sold"),
			new Appliance(counter.incrementAndGet(), "3132132143", "Hitachi", "CY601", "10/08/2018", "Used"),
			new Appliance(counter.incrementAndGet(), "6565436575", "Novita", "EY2018", "12/12/2019", "New"),
			new Appliance(counter.incrementAndGet(), "5345435465", "Philips", "CY625", "20/01/2010", "Old"),
			new Appliance(counter.incrementAndGet(), "5345435475", "Hitachi", "RK808A", "22/01/2018", "Unused"),
			new Appliance(counter.incrementAndGet(), "534543589", "Toyomi", "MRC20D", "22/01/2019", "Unused")
			));

	@GetMapping("/household")
	public List<Appliance> getAllAppliances() {
		return getAllItems();
	}
	
	
	@RequestMapping(method=RequestMethod.POST, value="/household")
	public ResponseEntity<Object> addAppliances(@RequestBody Appliance appliance) {
		if(addItems(appliance)) {
			return ResponseEntity.ok(HttpStatus.OK);
		}else{
			return ResponseEntity.ok(HttpStatus.FOUND);
		}
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/household")
	public void updateAppliances(@RequestBody Appliance appliance) {
		updateItems(appliance);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/household/{id}")
	public void deleteAppliances(@PathVariable int id) {
		deleteItems(id);
	}
	
	public List<Appliance> getAllItems(){		
		return availableItems;
	}
	
	public boolean addItems(Appliance appliance) {		
		long matchItem = availableItems.stream()
				.filter(t->t.getSerial_no().equals(appliance.getSerial_no()))
				.filter(t->t.getBrand().equals(appliance.getBrand()))
				.filter(t->t.getModel().equals(appliance.getModel()))
				.count() ;
		if(matchItem == 0) {
			appliance.setId(counter.incrementAndGet());
			availableItems.add(appliance);
			return true;
		}
		return false;		
		
	}
	
	public void updateItems(Appliance appliance) {	
		for (int i = 0; i < availableItems.size(); i++) {
			if (appliance.getId() == availableItems.get(i).getId()) {
				availableItems.set(i, appliance);				
            }
			
		}
		
	}
	
	public void deleteItems(long id) {	
		availableItems.removeIf(t-> ( t.getId() ==  id ));
		
	}
}
